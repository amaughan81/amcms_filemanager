AmcmsFilemanager::Engine.routes.draw do
  get :filemanager, to: 'filemanager#index'
  namespace :filemanager do
    get :change_directory
    post :mkdir
    post :copy
    post :upload
    put :rename
    delete :destroy
  end
  resources :imagemanager
end
