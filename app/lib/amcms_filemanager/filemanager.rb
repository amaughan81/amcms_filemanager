# frozen_string_literal: true

# Filemanager class for handling all things file related
class AmcmsFilemanager::Filemanager
  class << self
    # Make a copy of file in it's current directory
    # @param [String | nil] dir
    # @return [Sttring]
    def copy(filename)
      source_file = Rails.root.join('public', filename.gsub(/^\//, ''))

      return false if File.directory?(source_file)

      destination_dir = File.dirname(source_file)
      base_name = File.basename(source_file)

      destination_path = File.join(destination_dir, base_name)

      if File.exist?(destination_path)
        i = 1
        while File.exist?(destination_path)
          unique_name = "#{File.basename(source_file, '.*')}_#{i}#{File.extname(source_file)}"
          destination_path = File.join(destination_dir, unique_name)
          i += 1
        end
      end

      FileUtils.cp(source_file, destination_path)
      destination_path
    end

    def directory?(filename)
      source_file = Rails.root.join('public', filename.gsub(/^\//, ''))

      File.directory?(source_file)
    end

    # List all files in a given directory
    # @param [String | nil] dir
    # @param [String] type
    # @return [Array]
    def list(dir = nil, type = 'image')
      dir = AmcmsFilemanager.config.root_path unless dir.present?
      file_filter = AmcmsFilemanager.config.extension_allowlist.present? ? "{*/,*.{#{AmcmsFilemanager.config.extension_allowlist[type.to_sym].join(',')}}}" : '*'
      entries = Dir.glob(Rails.root.join('public', dir.gsub(%r{^/}, ''), file_filter))
      entries.map! { |entry| file_details(entry) }

      folders = entries.select { |entry| entry if entry[:directory] }.sort_by { |entry| entry[:filename] }.compact
      files = entries.reject { |entry| entry if entry[:directory] }.sort_by { |entry| entry[:filename] }.compact

      folders + files
    end

    # Return the parent directory
    # @param [String | nil] dir
    # @return [String]
    def parent_dir(dir = nil)
      dir = AmcmsFilemanager.config.root_path if !dir.present? || dir.blank?
      return dir if dir == AmcmsFilemanager.config.root_path

      File.dirname(dir).gsub(%r{^/}, '')
    end

    # Make a new directory
    # @param [String] dir
    # @param [String] directory_name
    # @return [Integer]
    def mkdir(dir, directory_name)
      dir = AmcmsFilemanager.config.root_path unless dir.present?
      Dir.mkdir(File.join(Rails.root.join('public', dir.gsub(%r{^/}, '')), directory_name), 0o775)
    end

    # Check to see if the directory exists
    # @param [String] dir
    # @param [String] filename
    # @return [TrueClass, FalseClass]
    def exists?(dir, filename)
      dir = AmcmsFilemanager.config.root_path unless dir.present?
      File.exist?(File.join(Rails.root.join('public', dir.gsub(%r{^/}, '')), filename))
    end

    # Delete a file or folder
    # @param [String] file
    # @return [Integer]
    def delete(file)
      filepath = Rails.root.join('public', file.gsub(%r{^/}, ''))

      return File.delete(filepath) unless File.directory?(filepath)

      FileUtils.remove_dir(filepath)
    end

    # Renames a file or directory
    # @param [String] dir
    # @param [String] original_filename
    # @param [String] new_filename
    # @return [Integer]
    def rename(dir, original_filename, new_filename)
      original_filepath = filepath(dir, original_filename)
      new_filepath = filepath(dir, new_filename)

      File.rename(original_filepath, new_filepath)
    end

    # Upload a file to the given directory
    # @param [String] dir
    # @param [ActionDispatch::Http::UploadedFile] file
    # @param [String] type
    def upload(dir, file, type = 'image')
      uploader = AmcmsFilemanager::FilemanagerUploader.new(type)
      uploader.define_singleton_method(:store_dir) do
        AmcmsFilemanager::Filemanager.filepath(dir, '')
      end
      uploader.store!(file)
    end

    # Create the filepath
    # @param [String] dir
    # @param [String] filename
    # @return [String]
    def filepath(dir, filename)
      dir = "#{AmcmsFilemanager.config.root_path}/#{dir}" unless dir.include? AmcmsFilemanager.config.root_path
      Rails.root.join('public', dir.gsub(%r{^/}, ''), filename)
    end

    # Detect if the file is an image
    # @param [File] file
    # @return [TrueClass, FalseClass]
    def image?(file)
      return false if File.directory?(file)

      web_safe_images = [
        'image/apng',
        'image/avif',
        'image/gif',
        'image/jpeg',
        'image/jpg',
        'image/png',
        'image/webp'
      ].freeze

      web_safe_images.include?(mimetype(file))
    end

    private

    # Present a hash of the file attributes
    # @param [String] file
    # @return [Hash{Symbol->TrueClass | FalseClass}]
    def file_details(file)
      {
        filename: File.basename(file),
        filepath: file.gsub(Rails.root.join('public').to_s, ''),
        directory: File.directory?(file),
        size: filesize(file),
        mimetype: mimetype(file),
        image: image?(file),
        date_modified: File.mtime(file).strftime('%d/%m/%Y %H:%M')
      }
    end

    # Get the mimetype of the file
    # @param [File] file
    # @return [String]
    def mimetype(file)
      return '' if File.directory?(file)

      Marcel::MimeType.for Pathname.new(file)
    end

    # Calculate the size of the file, returning the units of measure with the value
    # @param [String] file
    # @return [String (frozen)]
    def filesize(file)
      size = File.size(file) / 1000
      return "#{size} kb" if size < 1000

      "#{size / 1000} mb"
    end
  end
end
