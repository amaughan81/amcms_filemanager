module AmcmsFilemanager
  class ApplicationController < ActionController::Base
    before_action :authenticate

    def authenticate
      return unless AmcmsFilemanager.config.try(:authentication).present?

      send("authenticate_#{AmcmsFilemanager.config.authentication[:model]}!")
    end
  end
end
