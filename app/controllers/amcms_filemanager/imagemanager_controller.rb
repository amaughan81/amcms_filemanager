# frozen_string_literal: true

module AmcmsFilemanager
  class ImagemanagerController < ApplicationController
    def update
      uploader = AmcmsFilemanager::FilemanagerUploader.new
      uploader.retrieve_from_store!(source_file)
      uploader.resize_to_fit(params[:width], params[:height])
      render json: { success: true }
    rescue StandardError => e
      render json: { success: false, message: e.message }, status: 400
    end 

    def show
      if AmcmsFilemanager::Filemanager.image?(source_file)
        uploader = AmcmsFilemanager::FilemanagerUploader.new
        uploader.retrieve_from_store!(source_file)
        render json: { success: true, filepath: filepath, width: uploader.width, height: uploader.height, size: uploader.size }
      else
        render json: { success: false, error: 'File is not an image' }, status: 400
      end
    end

    private

    def source_file
      @source_file ||= params[:copy] ? AmcmsFilemanager::Filemanager.copy(filepath) : Rails.root.join('public', filepath)
    end

    def filepath
      "#{params[:id]}.#{params[:format]}".gsub(%r{^/}, '')
    end
  end
end
