# frozen_string_literal: true

module AmcmsFilemanager
  class FilemanagerController < ApplicationController
    before_action :item_exists?, only: %i[mkdir rename]

    def index
      @config = {
        routes: {
          cd_path: filemanager_change_directory_path,
          mkdir_path: filemanager_mkdir_path,
          rm_path: filemanager_destroy_path,
          rename_path: filemanager_rename_path,
          copy_path: filemanager_copy_path,
          image_info_path: imagemanager_path(':id'),
        },
        type: params.fetch(:type, 'all')
      }
    end

    def change_directory
      @files = AmcmsFilemanager::Filemanager.list(params[:cd], params[:type])

      render json: {
        files: @files,
        pwd: pwd,
        pwd_name: pwd.split('/').last,
        root_directory: AmcmsFilemanager.config.root_path,
        parent_directory: AmcmsFilemanager::Filemanager.parent_dir(params[:cd])
      }
    end

    def copy
      if AmcmsFilemanager::Filemanager.copy(params[:item_name])
        render json: { success: true }, status: 200
      else
        render json: { success: false }, status: 400
      end
    end

    def mkdir
      if AmcmsFilemanager::Filemanager.mkdir(params[:pwd], params[:item_name])
        render json: { success: true }, status: 200
      else
        render json: { success: false }, status: 400
      end
    end

    def rename
      if AmcmsFilemanager::Filemanager.rename(params[:pwd], params[:original_filename], params[:item_name])
        render json: { success: true }, status: 200
      else
        render json: { success: false }, status: 400
      end
    end

    def destroy
      if AmcmsFilemanager::Filemanager.delete(params[:file])
        render json: { success: true }, status: 200
      else
        render json: { success: false }, status: 400
      end
    end

    def upload
      if AmcmsFilemanager::Filemanager::upload(params[:dir], params[:file], params[:type])
        render json: { success: true }, status: 200
      else
        render json: { success: false }, status: 400
      end
    rescue CarrierWave::IntegrityError => e
      render json: { error: e.message }, status: 400
    end

    private

    def item_exists?
      return true unless AmcmsFilemanager::Filemanager.exists?(params[:pwd], params[:item_name])

      message = {
        title: 'Error',
        body: 'Item already exists. Please try a different name',
        status: 'danger'
      }
      render json: { success: false, message: message }, status: 400
      false
    end

    def pwd
      @pwd ||= params[:cd].blank? ? AmcmsFilemanager.config.root_path : params[:cd].gsub(%r{^/}, '')
    end
  end
end
