# frozen_string_literal: true

module AmcmsFilemanager
  class Engine < ::Rails::Engine
    isolate_namespace AmcmsFilemanager

    initializer 'amcms_filemanager.assets.precompile' do |app|
      app.config.assets.precompile << 'amcms_filemanager_manifest.js'
    end
  end

  class << self
    def configure(&block)
      @config ||= AmcmsFilemanager::Engine::Configuration.new

      yield @config if block

      @config
    end

    def config
      Rails.application.config
    end
  end
end
