# frozen_string_literal: true

module AmcmsFilemanager
  class Configuration
    attr_accessor :root_path
    attr_accessor :authentication
    attr_accessor :extension_allowlist
  end
end
