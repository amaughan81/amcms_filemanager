require_relative "lib/amcms_filemanager/version"

Gem::Specification.new do |spec|
  spec.name        = "amcms_filemanager"
  spec.version     = AmcmsFilemanager::VERSION
  spec.authors     = ["Andrew Maughan"]
  spec.email       = ["andrew@maughan@org.uk"]
  spec.homepage    = "https://www.amaughan.com"
  spec.summary     = "Summary of AmcmsFilemanager."
  spec.description = "Description of AmcmsFilemanager."
  spec.license     = "MIT"

  # Prevent pushing this gem to RubyGems.org. To allow pushes either set the 'allowed_push_host'
  # to allow pushing to a single host or delete this section to allow pushing to any host.
  # spec.metadata["allowed_push_host"] = "TODO: Set to 'http://mygemserver.com'"

  spec.metadata["homepage_uri"] = spec.homepage
  # spec.metadata["source_code_uri"] = "TODO: Put your gem's public repo URL here."
  # spec.metadata["changelog_uri"] = "TODO: Put your gem's CHANGELOG.md URL here."

  spec.files = Dir["{app,config,db,lib}/**/*", "MIT-LICENSE", "Rakefile", "README.md"]

  spec.add_dependency 'carrierwave'
  spec.add_dependency 'carrierwave-i18n'
  spec.add_development_dependency "rspec-rails"
  spec.add_development_dependency 'faker'
end
