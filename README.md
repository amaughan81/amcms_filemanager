# AmcmsFilemanager
Filemanager for tinymce.  Allows uploading of files to a remote server via the file picker plugin in Tinymce.

## Full Feature List
- Browse directory
- Mutli File Upload
- Rename File
- Copy File
- Delete File
- Make Directory
- Resize Image
- Authentication hooks
- Allowed file types

## Usage
How to use my plugin.

## Installation
Add this line to your application's Gemfile:

```ruby
gem 'amcms_filemanager'
```

And then execute:
```bash
$ bundle
```

Or install it yourself as:
```bash
$ gem install amcms_filemanager
```

Mount the filemanger routes in `config/routes.rb`

```ruby
mount AmcmsFilemanager::Engine => '/', as: :amcms_filemanager
```

Create an initialiser in your project

`config/initialisers/amcms_filemanager.rb`


```ruby
AmcmsFilemanager::configure do |config|
  config.root_path = 'images'
  config.authentication = {
    model: :user
  }
end
```

In your project where TinyMCE is located add this to initialise the filemanager:
```js
const amcmsFileManager = (type, callback) => tinymce.activeEditor.windowManager.openUrl({
  title: 'AMCMS Filemanager',
  url: `http://localhost:3000/filemanager?type=${type}`,
  onMessage: function(instance, event) {
    callback(event.data.location);
    tinymce.activeEditor.windowManager.close();
  },
  width: window.innerWidth - 50,
  height: window.innerHeight - 50,
});
```

Next add a callback to the filepicker to trigger the filemanager for image, link and media dialogs:

```js
tinymce.init({
  ...
  file_picker_callback: (callback, value, meta) => {
    amcmsFileManager(meta.filetype, callback);
  }
});
```