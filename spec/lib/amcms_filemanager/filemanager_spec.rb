# frozen_string_literal: true

require 'rails_helper'

RSpec.describe AmcmsFilemanager::Filemanager do
  describe '#list' do
    it 'returns an array of file hashes' do
      files = AmcmsFilemanager::Filemanager.list
      expect(files).to be_an(Array)
      expect(files.first).to be_an(Hash)
    end

    it 'returns details for a file' do
      files = AmcmsFilemanager::Filemanager.list
      file = files.first
      keys = %i[filename filepath directory size date_modified]
      keys.each do |key|
        expect(file.key?(key)).to be_truthy
      end
    end
  end

  describe '#parent_directory' do
    context 'when a directory path is not passed' do
      it 'returns the root_path' do
        expect(AmcmsFilemanager::Filemanager.parent_dir).to eq(AmcmsFilemanager.config.root_path)
      end
    end

    context 'when a directory path is passed' do
      it 'returns the parent_directory' do
        expect(AmcmsFilemanager::Filemanager.parent_dir('/images/thumbs')).to eq('thumbs')
      end
    end
  end

  describe '#mkdir' do
    it 'creates a directory in the provided path' do
      folder = "#{Rails.root}/public/images/folder"
      Dir.delete(folder) if Dir.exist?(folder)
      AmcmsFilemanager::Filemanager.mkdir('/images', 'folder')
      expect(Dir.exist?("#{Rails.root}/public/images/folder")).to be_truthy
    end
  end

  describe '#delete' do
    context 'when the item is a file' do
      it 'deletes the file' do
        filename = "#{Rails.root}/public/images/img.png"
        destination = "#{Rails.root}/public/images/img2.png"
        FileUtils.cp(filename, destination)

        expect(File.exist?(destination)).to be_truthy
        AmcmsFilemanager::Filemanager.delete('/images/img2.png')
        expect(File.exist?(destination)).to be_falsey
      end
    end

    context 'when the item is a folder' do
      it 'deletes the folder' do
        folder = "#{Rails.root}/public/images/folder_to_delete"
        Dir.mkdir(folder)
        expect(Dir.exist?(folder)).to be_truthy
        AmcmsFilemanager::Filemanager.delete('/images/folder_to_delete')
        expect(Dir.exist?(folder)).to be_falsey
      end
    end
  end

  describe '#rename' do
    it 'renames the item' do
      filename = "#{Rails.root}/public/images/img.png"
      original_filepath = "#{Rails.root}/public/images/img2.png"
      new_filepath = "#{Rails.root}/public/images/renamed.png"
      File.delete(original_filepath) if File.exist?(original_filepath)
      File.delete(new_filepath) if File.exist?(new_filepath)
      FileUtils.cp(filename, original_filepath)

      AmcmsFilemanager::Filemanager.rename('/images', 'img2.png', 'renamed.png')
      expect(File.exist?(original_filepath)).to be_falsey
      expect(File.exist?(new_filepath)).to be_truthy
    end
  end
end
