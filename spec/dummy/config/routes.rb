Rails.application.routes.draw do
  mount AmcmsFilemanager::Engine => "/amcms_filemanager", as: :amcms_filemanager
end
