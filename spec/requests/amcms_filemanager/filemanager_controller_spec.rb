# frozen_string_literal: true

require 'rails_helper'

RSpec.describe AmcmsFilemanager::FilemanagerController, type: :request do
  describe '#index' do
    it 'renders the page successfully' do
      get amcms_filemanager.filemanager_path
      expect(response).to have_http_status(:success)
    end
  end

  describe '#change_directory' do
    it 'returns a json response' do
      get amcms_filemanager.filemanager_change_directory_path
      expect(JSON.parse(response.body)).to be_a(Hash)
    end
  end

  describe 'mkdir' do
    before(:each) do
      @dir = "#{Rails.root}/public/images/folder"
      Dir.delete(@dir) if Dir.exist?(@dir)
    end

    context 'when the directory does not exist' do
      it 'creates it in the directory provided' do
        post amcms_filemanager.filemanager_mkdir_path, params: { pwd: 'images', item_name: 'folder' }
        expect(Dir.exist?(@dir)).to be_truthy
      end

      it 'returns a json response with success true' do
        post amcms_filemanager.filemanager_mkdir_path, params: { pwd: 'images', item_name: 'folder' }
        expect(response).to have_http_status(:success)
        json_response = JSON.parse(response.body)
        expect(json_response['success']).to be_truthy
      end
    end

    context 'when there was a problem creating the directory' do
      before(:each) do
        allow(AmcmsFilemanager::Filemanager).to receive(:mkdir).and_return(false)
      end

      it 'returns a bad request' do
        post amcms_filemanager.filemanager_mkdir_path, params: { pwd: 'images', item_name: 'folder' }
        expect(response).to have_http_status(:bad_request)
      end

      it 'returns a json response with success false' do
        post amcms_filemanager.filemanager_mkdir_path, params: { pwd: 'images', item_name: 'folder' }
        json_response = JSON.parse(response.body)
        expect(json_response['success']).to be_falsey
      end
    end

    context 'when the directory already exists' do
      before(:each) do
        allow(AmcmsFilemanager::Filemanager).to receive(:exists?).and_return(:false)
      end

      it 'returns a json response with success false with an error message' do
        post amcms_filemanager.filemanager_mkdir_path, params: { pwd: 'images', item_name: 'folder' }
        expect(response).to have_http_status(:bad_request)
        json_response = JSON.parse(response.body)
        expect(json_response['success']).to be_falsey
        expect(json_response['message'].present?).to be_truthy
      end

      it 'returns an error message with three keys' do
        post amcms_filemanager.filemanager_mkdir_path, params: { pwd: 'images', item_name: 'folder' }
        expect(response).to have_http_status(:bad_request)
        json_response = JSON.parse(response.body)

        keys = %w[title body status]
        keys.each do |key|
          expect(json_response['message'].key?(key)).to be_truthy
        end
      end
    end
  end

  describe '#destroy' do
    context 'when the request is successful' do
      it 'returns a json object with success true and status 200' do
        allow(AmcmsFilemanager::Filemanager).to receive(:delete).and_return(true)
        delete amcms_filemanager.filemanager_destroy_path, params: { file: '/images/img2.png' }

        json_response = JSON.parse(response.body)
        expect(json_response['success']).to be_truthy
        expect(response).to have_http_status(:success)
      end
    end

    context 'when the request is unsuccessful' do
      it 'returns a json object with success false and status 400' do
        allow(AmcmsFilemanager::Filemanager).to receive(:delete).and_return(false)
        delete amcms_filemanager.filemanager_destroy_path, params: { file: '/images/img2.png' }

        json_response = JSON.parse(response.body)
        expect(json_response['success']).to be_falsey
        expect(response).to have_http_status(:bad_request)
      end
    end
  end

  describe 'rename' do
    context 'when the the renamed file already exists' do
      before(:each) do
        allow(AmcmsFilemanager::Filemanager).to receive(:exists?).and_return(:false)
      end

      it 'returns success false with an error message and status bad request' do
        put amcms_filemanager.filemanager_rename_path, params: { pwd: 'images', item_name: 'renamed.jpg' }
        expect(response).to have_http_status(:bad_request)
        json_response = JSON.parse(response.body)
        expect(json_response['success']).to be_falsey
        expect(json_response['message'].present?).to be_truthy
      end
    end
  end
end
